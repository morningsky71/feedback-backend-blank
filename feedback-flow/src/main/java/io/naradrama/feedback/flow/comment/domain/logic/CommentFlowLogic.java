/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.flow.comment.domain.logic;

import io.naradrama.feedback.aggregate.comment.domain.entity.Comment;
import io.naradrama.feedback.aggregate.comment.domain.entity.sdo.CommentCdo;
import io.naradrama.feedback.aggregate.comment.domain.logic.CommentLogic;
import io.naradrama.feedback.flow.comment.api.command.command.RegisterCommentCommand;
import io.naradrama.feedback.flow.comment.api.command.command.RemoveCommentCommand;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class CommentFlowLogic {
    //
    private final CommentLogic commentLogic;

    public RegisterCommentCommand registerComment(RegisterCommentCommand command) {
        //
        CommentCdo commentCdo = command.getCommentCdo();
        String commentId;

        try {

            commentId = commentLogic.registerComment(commentCdo);
            command.setCommandResponse(new CommandResponse(commentId));

        }catch(Exception e){
             CommandResponse cr = new CommandResponse();
             cr.setResult(false);
             command.setCommandResponse(cr);
        }


        return command;
    }

    public RemoveCommentCommand removeComment(RemoveCommentCommand command) {
        //
        String commentId = command.getCommentId();
        Comment comment = commentLogic.findComment(commentId);

        comment.markDeleted();
        commentLogic.modifyComment(comment);

        command.setCommandResponse(new CommandResponse(commentId));
        return command;
    }
}
