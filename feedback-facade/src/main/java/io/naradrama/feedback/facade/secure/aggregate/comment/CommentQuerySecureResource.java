/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.facade.secure.aggregate.comment;

import io.naradrama.feedback.aggregate.comment.api.query.query.CommentDynamicQuery;
import io.naradrama.feedback.aggregate.comment.api.query.query.CommentQuery;
import io.naradrama.feedback.aggregate.comment.api.query.query.CommentsDynamicQuery;
import io.naradrama.feedback.aggregate.comment.api.query.rest.CommentQueryFacade;
import io.naradrama.feedback.aggregate.comment.store.CommentStore;
import io.naradrama.feedback.aggregate.comment.store.maria.jpo.CommentJpo;
import io.naradrama.prologue.util.json.JsonUtil;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.naraplatform.daysman.daysboy.config.Daysboy;

import javax.persistence.EntityManager;

// TODO Extract Apis from CommentQueryResource which are used by front project
//  url
//   1. Replace 'aggregate' to 'secure' of origin url (e.g. /aggregate/comment/~ => /secure/comment/~)

@RestController
@RequestMapping(value ="/secure/comment/query")
public class CommentQuerySecureResource implements CommentQueryFacade {

    private final CommentStore commentStore;
    private final RdbQueryRequest<CommentJpo> request;

    public CommentQuerySecureResource(CommentStore commentStore, EntityManager  entityManager) {
        this.commentStore = commentStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @Daysboy
    @RequestMapping(value ="/aggregate/comment/query")
    public CommentQuery execute(@RequestBody CommentQuery commentQuery)  {
        commentQuery.execute(commentStore);
        return commentQuery;
    }

    @Daysboy
    @Override
    @PostMapping(value="/dynamic-single")
    public CommentDynamicQuery execute(@RequestBody CommentDynamicQuery commentDynamicQuery) {
        commentDynamicQuery.execute(request);
        return commentDynamicQuery;
    }

    @Daysboy
    @Override
    @PostMapping(value="/dynamic-multi")
    public CommentsDynamicQuery execute(@RequestBody CommentsDynamicQuery commentsDynamicQuery) {
        System.out.println(JsonUtil.toJson(request.toString()));
        commentsDynamicQuery.execute(request);

        return commentsDynamicQuery;
    }

}
