/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.aggregate.comment.api.command.rest;

import io.naradrama.feedback.aggregate.comment.api.command.command.CommentCommand;
import io.naradrama.feedback.aggregate.comment.domain.logic.CommentLogic;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// TODO Implement RestController Component which implements CommandFacade and receives CommentLogic as Dependency Injection
//  url
//  1. root : /aggregate/comment
//  2. command with Comment : /comment/command

@RestController
@RequestMapping(value ="/aggregate/comment")
public class CommentResource implements CommentFacade{

    private final CommentLogic commentLogic;

    public CommentResource(CommentLogic commentLogic) {
        this.commentLogic = commentLogic;
    }

    @Override
    @PostMapping(value="/comment/command")
    public CommentCommand executeComment(@RequestBody CommentCommand commentCommand) {

        CommentCommand ret =null;
        try{
           ret =  commentLogic.routeCommand(commentCommand);
        }catch(Exception e){
            e.getMessage();
        }
        return ret;
    }

}
