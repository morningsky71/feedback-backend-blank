/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.aggregate.comment.store.maria;

import io.naradrama.feedback.aggregate.comment.domain.entity.Comment;
import io.naradrama.feedback.aggregate.comment.store.CommentStore;
import io.naradrama.feedback.aggregate.comment.store.maria.jpo.CommentJpo;
import io.naradrama.feedback.aggregate.comment.store.maria.repository.CommentMariaRepository;
import io.naradrama.prologue.domain.Offset;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

// TODO Implement Repository Component which implements CommentStore and receives CommentMariaRepository as Dependency Injection
@Repository
public class CommentMariaStore implements CommentStore{

    private final CommentMariaRepository commentMariaRepository;

    public CommentMariaStore(CommentMariaRepository commentMariaRepository) {
        this.commentMariaRepository = commentMariaRepository;
    }

    //
    private Pageable createPageable(Offset offset) {
        /* Autogen by nara studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }

    @Override
    public void create(Comment comment) {

        CommentJpo commentJpo = new CommentJpo(comment);
        commentMariaRepository.save(commentJpo);
    }

    @Override
    public Comment retrieve(String id) {

        Optional<CommentJpo>  commentJpo = this.commentMariaRepository.findById(id);

        if(commentJpo.isPresent()){
            return commentJpo.get().toDomain();
        }else{
            return null;
        }
    }

    @Override
    public List<Comment> retrieveAll(Offset offset) {

        /* Autogen by nara studio */
        Pageable pageable = createPageable(offset);
        Page<CommentJpo> page = commentMariaRepository.findAll(pageable);
        offset.setTotalCount(page.getTotalElements());
        List<CommentJpo> commentJpo = page.getContent();

        return CommentJpo.toDomains(commentJpo);
    }

    @Override
    public void update(Comment comment) {

        CommentJpo commentJpo = new CommentJpo(comment);
        this.commentMariaRepository.save(commentJpo);
    }

    @Override
    public void delete(Comment comment) {
        CommentJpo commentJpo = new CommentJpo(comment);
        this.commentMariaRepository.delete(commentJpo);
    }

    @Override
    public void delete(String id) {
        this.commentMariaRepository.deleteById(id);

    }

    @Override
    public boolean exists(String id) {

        Optional<CommentJpo>  commentJpo = this.commentMariaRepository.findById(id);
        if(commentJpo.isPresent()){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public List<Comment> retrieveAllByFeedbackId(String feedbackId) {

        List<CommentJpo> commentJpos = (List) this.commentMariaRepository.findAll();
        List<Comment> comments = (List)commentJpos
                .stream()
                .map(commentJpo -> commentJpo.toDomain().getFeedbackId().equals(feedbackId))
                .collect(Collectors.toList());

        return comments;
    }

    @Override
    public List<Comment> deleteByFeedbackId(String feedbackId) {

//        List<CommentJpo> commentJpos = (List) this.commentMariaRepository.findAll();
//        List<Comment> comments = (List)commentJpos
//                .stream()
//                .map(commentJpo -> commentJpo.toDomain().getFeedbackId().equals(feedbackId))
//                .collect(Collectors.toList());
//        this.commentMariaRepository.deleteAll((Iterable)comments);

          this.commentMariaRepository.deleteByFeedbackId(feedbackId);

        return null;
    }

    public CommentMariaRepository getCommentMariaRepository() {
        return commentMariaRepository;
    }
}
