/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.aggregate.comment.api.command.rest;

import io.naradrama.feedback.aggregate.comment.api.command.command.CommentCommand;

public interface CommentFacade {
    /* Autogen by nara studio */
    CommentCommand executeComment(CommentCommand commentCommand);
}
