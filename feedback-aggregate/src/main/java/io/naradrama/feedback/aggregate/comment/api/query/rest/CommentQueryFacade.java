/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.aggregate.comment.api.query.rest;

import io.naradrama.feedback.aggregate.comment.api.query.query.CommentDynamicQuery;
import io.naradrama.feedback.aggregate.comment.api.query.query.CommentQuery;
import io.naradrama.feedback.aggregate.comment.api.query.query.CommentsDynamicQuery;

public interface CommentQueryFacade {
    /* Autogen by nara studio */
    CommentQuery execute(CommentQuery commentQuery);
    CommentDynamicQuery execute(CommentDynamicQuery commentDynamicQuery);
    CommentsDynamicQuery execute(CommentsDynamicQuery commentsDynamicQuery);
}
