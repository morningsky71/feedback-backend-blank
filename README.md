# Feedback

## Domain Model

### Comment

```plantuml

hide circle
hide methods

skinparam classAttributeIconSize 0
skinparam linetype polyline
skinparam linetype ortho

entity CommentFeedback <<DomainAggregate>> <<DomainEntity>> #FDC {
    config : CommentConfig
    title : String
    time : long
    sourceEntityId : String
    <i>*comments : List<Comment>
}

entity CommentConfig <<ValueObject>> #EFE {
    anonymous : boolean
    maxCommentCount : int
    maxCommentMessag : int
    maxSubCommentCou : int
    maxSubCommentMes : int
    embeddedSubC : boolean
    maxEmbeddedSubCo : int
    deletable : boolean
}

entity Comment <<DomainAggregate>> <<DomainEntity>> #FDC {
    writerId : String
    writerName : String
    message : String
    base64AttachedImage : String
    feedbackId : String
    important : boolean
    deleted : boolean
    time : long
    subCommentCount : int
    embeddedSubComments : EmbeddedSubCommentList
    <i>*subComments : private List<SubComment>
}

entity SubComment <<DomainEntity>> {
    writerId : String
    writerName : String
    message : String
    base64Atta : String
    commentId : String
    time : long
    deleted : boolean
}

entity EmbeddedSubCommentList <<ValueObject>> #EFE {
    embeddedSubComments : List<EmbeddedSubComment>
}

entity EmbeddedSubComment <<ValueObject>> #EFE {
    writerId : String
    writerName : String
    message : String
    base64AttachedImage : String
    time : long
    deleted : boolean
}

CommentFeedback -down-> "n" Comment
CommentFeedback -right- CommentConfig
Comment -down-> "n" SubComment
Comment -right- EmbeddedSubCommentList
EmbeddedSubCommentList -down-> "n" EmbeddedSubComment

```

### Report

```plantuml

hide circle
hide methods

skinparam classAttributeIconSize 0
skinparam linetype polyline
skinparam linetype ortho

entity ReportFeedback <<DomainAggregate>> <<DomainEntity>> #FDC {
    config : ReportConfig
    title : String
    sourceEntityId : String
    time : long
    <i>*reports : List<Report>
}

entity ReportConfig <<ValueObject>> #EFE {
    anonymous : boolean
    maxReportCount : int
    maxReportMessageLength : int
}

entity Report <<DomainAggregate>> <<DomainEntity>> #FDC {
    writerId : String
    writerName : String
    title : String
    message : String
    capturedImageId : String
    fileBoxId : String
    feedbackId : String
    correctActions : List<CorrectAction>
    time : long
}

entity CorrectAction <<ValueObject>> #EFE {
    writerId : String
    writerName : String
    message : String
    time : long
}

ReportFeedback -right- ReportConfig
ReportFeedback -down-> "n" Report
Report -right-> "n" CorrectAction

```

### Review

```plantuml

hide circle
hide methods

skinparam classAttributeIconSize 0
skinparam linetype polyline
skinparam linetype ortho

entity ReviewFeedback <<DomainAggregate>> <<DomainEntity>> #FDC {
    config : ReviewConfig
    title : String
    sourceEntityId : String
    time : long
    <i>*reviewSummary : ReviewSummary
    <i>*reviews : List<Review>
}

entity ReviewConfig <<ValueObject>> #EFE {
    maxStarCount : int
    versionBased : boolean
    anonymous : boolean
    maxReviewCount : int
    maxReviewOpinionLength : int
}

entity ReviewSummary <<DomainEntity>> {
    versionBased : boolean
    maxStarCount : int
    feedbackId : String
    starCounts : List<IntPair>
    average : double
    reviewCount : int
    versionStarCountMap : Map<String, List>
    versionAverageMap : Map<String, Double>
    versionReviewCountMap : Map<String, Integer>
}

entity Review <<DomainAggregate>> <<DomainEntity>> #FDC {
    writerId : String
    writerName : String
    title : String
    opinion : String
    selectedStar : int
    version : String
    feedbackId : String
    time : long
    helpCountPair : IntPair
    <i>*helpComments : List<HelpComment>
}

entity HelpComment <<DomainEntity>> {
    anonymous : boolean
    writerId : String
    helpful : boolean
    reviewId : String
    time : long
}

ReviewFeedback -right- ReviewConfig
ReviewFeedback -left- ReviewSummary
ReviewFeedback -down-> "n" Review
Review -right-> "n" HelpComment

```